import firebase from "@firebase/app";
import "@firebase/database";

try {
  firebase.initializeApp({
    databaseURL: "https://hacker-news.firebaseio.com"
  });
} catch (err) {
  console.error("Firebase initialization error", err.stack);
}

export const db = firebase.database().ref("v0");
