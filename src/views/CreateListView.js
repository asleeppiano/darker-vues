import ItemList from "./ItemList.vue";

const camelize = str => str.charAt(0).toUpperCase() + str.slice(1);

// This is a factory function for dynamically creating root-level list views,
// since they share most of the logic except for the type of items to display.
// They are essentially higher order components wrapping ItemList.vue.
export default function createListView(category) {
  return {
    name: `${category}-view`,

    created() {
      console.log(this.$route.params, category);
      return this.$store.dispatch("getStories", {
        category,
        page: this.$route.params.page || 1
      });
    },

    title: camelize(category),

    render(h) {
      return h(ItemList, { props: { category } });
    }
  };
}
