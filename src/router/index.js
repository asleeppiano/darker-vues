import Vue from "vue";
import VueRouter from "vue-router";
import createListView from "@/views/CreateListView.js";
import Comments from "@/views/Comments.vue";
import User from "@/views/User.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/topstories/:page(\\d+)?",
    name: "topstories",
    component: createListView("topstories")
  },
  {
    path: "/newstories/:page(\\d+)?",
    name: "newstories",
    component: createListView("newstories")
  },
  {
    path: "/beststories/:page(\\d+)?",
    name: "beststories",
    component: createListView("beststories")
  },
  {
    path: "/showstories/:page(\\d+)?",
    name: "showstories",
    component: createListView("showstories")
  },
  {
    path: "/askstories/:page(\\d+)?",
    name: "askstories",
    component: createListView("askstories")
  },
  {
    path: "/jobstories/:page(\\d+)?",
    name: "jobstories",
    component: createListView("jobstories")
  },
  { path: "/", redirect: "/topstories" },
  {
    path: "/comments/:postid",
    name: "Comments",
    component: Comments
  },
  {
    path: "/user/:userid",
    name: "User",
    component: User
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  scrollBehavior: () => ({ y: 0 }),
  routes
});

export default router;
