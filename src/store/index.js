import Vue from "vue";
import Vuex from "vuex";
import { db } from "../firebase.js";

Vue.use(Vuex);

const STORIES_PER_PAGE = 15;

function fetchItem(kid) {
  return new Promise((resolve, reject) => {
    db.child(`item/${kid}`).once(
      "value",
      snapshot => {
        const val = snapshot.val();
        resolve(val);
      },
      reject
    );
  });
}

function fetchItems(ids) {
  return Promise.all(ids.map(id => fetchItem(id)));
}

export default new Vuex.Store({
  state: {
    currentPost: {},
    newsList: [],
    commentList: {},
    currentPage: 0,
    maxPage: 0,
    fetchStatus: null,
    user: ""
  },
  mutations: {
    setCurrentPost(state, post) {
      state.currentPost = post;
    },
    setNewsList(state, list) {
      state.newsList = list;
    },
    setCommentList(state, comments) {
      comments.forEach(comment => {
        if (comment) {
          Vue.set(state.commentList, comment.id, comment);
        }
      });
    },
    incrementCurrentPage(state) {
      state.currentPage++;
    },
    decrementCurrentPage(state) {
      state.currentPage--;
    },
    setFetchStatus(state, status) {
      state.fetchStatus = status;
    },
    setUser(state, user) {
      state.user = user;
    },
    setMaxPage(state, length) {
      state.maxPage = length / STORIES_PER_PAGE;
    }
  },
  actions: {
    getStories(context, { category, page }) {
      let arr = [];
      console.log("page", page * STORIES_PER_PAGE);
      context.commit("setFetchStatus", "loading");
      db.child(category)
        .once("value")
        .then(snapshot => {
          context.commit("setMaxPage", snapshot.val().length);
          Promise.all(
            snapshot
              .val()
              .slice((page - 1) * STORIES_PER_PAGE, page * STORIES_PER_PAGE)
              .map(id => {
                return db.child(`item/${id}`).once("value");
              })
          )
            .then(postidList => {
              postidList.map(s => {
                arr.push(s.val());
              });
              return arr;
            })
            .then(arr => {
              context.commit("setNewsList", arr);
              context.commit("setFetchStatus", "successful");
            })
            .catch(e => {
              console.log(e);
              context.commit("setFetchStatus", "failed");
            });
        })
        .catch(e => {
          console.log(e);
        });
    },
    async fetchComments({ commit, dispatch }, item) {
      if (!item) {
        return;
      }
      return fetchItems(item.kids || []).then(comments => {
        commit("setCommentList", comments);
        return Promise.all(
          comments.map(item => {
            dispatch("fetchComments", item);
          })
        );
      });
    },
    async getPost({ state, commit }, id) {
      const post = state.newsList.find(news => {
        return news.id === id;
      });
      if (post) {
        commit("setCurrentPost", post);
      } else {
        db.child(`item/${id}`)
          .once("value")
          .then(post => {
            commit("setCurrentPost", post.val());
          });
      }
    },
    async fetchUser({ state, commit }, id) {
      if (state.user.id === id) {
        return;
      }
      console.log("fetchUser", id);
      db.child(`user/${id}`)
        .once("value")
        .then(post => {
          console.log("post", post.val());
          commit("setUser", post.val());
          return;
        });
    }
  },
  modules: {}
});
