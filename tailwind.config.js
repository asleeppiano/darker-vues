module.exports = {
  theme: {
    fontFamily: {
      sans: [
        "Manrope",
        "system-ui",
        "-apple-system",
        "BlinkMacSystemFont",
        '"Segoe UI"',
        "Roboto",
        '"Helvetica Neue"',
        "Arial",
        '"Noto Sans"',
        "sans-serif",
        '"Apple Color Emoji"',
        '"Segoe UI Emoji"',
        '"Segoe UI Symbol"',
        '"Noto Color Emoji"'
      ],
      serif: ["Georgia", "Cambria", '"Times New Roman"', "Times", "serif"],
      mono: [
        "Menlo",
        "Monaco",
        "Consolas",
        '"Liberation Mono"',
        '"Courier New"',
        "monospace"
      ]
    },
    extend: {
      colors: {
        main: "var(--color-main)",
        "main-a70": "var(--color-main-transparent)",
        secondary: "var(--color-secondary)",
        accent: "var(--color-accent)",
        "accent-a70": "var(--color-accent-transparent)"
      }
    }
  },
  variants: {
    margin: ["responsive", "hover", "focus", "first", "last"]
  },
  plugins: []
};
